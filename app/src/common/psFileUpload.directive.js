/**
 * @ngdoc directive
 * @name app.common.directive:psFileUpload
 * @scope true
 * @param {object} test test object
 * @restrict E
 *
 * @description < description placeholder >
 *
 */

(function () {

    'use strict';

    angular
        .module('app.common')
        .directive('psFileUpload', psFileUpload);

    /* @ngInject */
    function psFileUpload() {

        return {
            link: link,
            restrict: 'E',
            templateUrl: 'src/common/psFileUpload.html',
            controller: function () {
                var vm = this;

                // Put all state related stuff here
                vm.properties = {
                    previewReady: false
                };

                // Put all scope data here

                vm.data = {
                    videoId: null,
                    videoEmbedClass: null
                };

                vm.options = {
                    url: 'https://upload.wistia.com',
                    autoUpload: true,
                    singleFileUploads: true,
                    limitMultiFileUploads: 1
                };

            },
            controllerAs: 'vm'

        };

        /////////////////////

        function link(scope, elem, attrs) {
            scope.$on('fileuploaddone', function (e, data) {
                // Your code here
                //            console.log('meow', e, data, data.result);
                scope.vm.properties.previewReady = true;
                scope.vm.data.videoId = data.result.hashed_id;
                scope.vm.data.videoEmbedClass = "wistia_async_" + data.result.hashed_id;
            });

            scope.$on('fileuploadchange', function (e, data) {
                // Your code here
                //            console.log('meow', e, data, data.result);
                scope.vm.properties.previewReady = false;

            });

        }
    }

}());